package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "ramses" ) );
	}

	@Test
	public void testHasOnlyAlphaCharacterOrNumberRegular() {
		assertTrue("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ramses"));
	}
	
	@Test
	public void testHasOnlyAlphaCharacterOrNumberException() {
		assertFalse("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ramses_@"));
	}	

	@Test
	public void testHasOnlyAlphaCharacterOrNumberBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ramses1"));
	}

	@Test
	public void testHasOnlyAlphaCharacterOrNumberBoundaryOut() {
		assertFalse("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ramses~"));
	}	

	@Test
	public void testHasAtLeastSixAlphanumericCharactersRegular() {
		assertTrue("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ramses123"));
	}
	
	@Test
	public void testHasAtLeastSixAlphanumericCharactersException() {
		assertFalse("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ram~"));
	}
	
	@Test
	public void testHasAtLeastSixAlphanumericCharactersBoundaryIn() {
		assertTrue("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ramse1"));
	}
	
	@Test
	public void testHasAtLeastSixAlphanumericCharactersBoundaryOut() {
		assertTrue("Invalid login", LoginValidator.hasOnlyAlphaCharacterOrNumber("ramse"));
	}
}
