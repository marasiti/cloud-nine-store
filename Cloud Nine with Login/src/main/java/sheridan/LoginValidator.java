package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		if(hasOnlyAlphaCharacterOrNumber(loginName) && hasAtLeastSixAlphanumericCharacters(loginName)) {
			return true;			
		}
		
		return false;
			
	}
	
	public static boolean hasOnlyAlphaCharacterOrNumber(String loginName) {
		
		for(int i = 0; i < loginName.length(); i++) {
			//If character is not an alpha character or digit return false
			if(!Character.isLetterOrDigit(loginName.charAt(i))) {
				return false;
			}
		}
		
		//doesn't fail so return true
		return true;
	}
	
	public static boolean hasAtLeastSixAlphanumericCharacters(String loginName) {
		if(!hasOnlyAlphaCharacterOrNumber(loginName)) {
			return false;
		}
		
		if(loginName.length() < 6) {
			return false;
		}
		
		return true;
	}
}
